# Experian Ansible Demo

The purpose of this repository is to setup a demo of Ansible for Experian. It is a minimalistic approach as a POC to show them how it works.

## Setup

There are a few prerequisites that need to be addressed prior to running the Ansible code.

First of all - Install Ansible. Follow the instructions here: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html


## Executing a Playbook

There are three playbooks provided here:
1. `datadog_playbook.yaml` installs the Datadog agent on both machines
2. `sqlserver_playbook.yaml` sets up the Datadog sqlserver user

The Datadog agent sets up configurations based on whats in that playbook, as well as the variables that are found in the `group_vars` directory. This way, we can install the `windows_service` and `sqlserver`
integrations for the Windows server, and `nginx` integration for the webserver, while keeping that information separate.


The sqlserver playbook will fail, and that's intentional. It fails because the user is already created. The idea here is to show how failures are descriptive and useful.

To run any of these, run the following command:
```
ansible-playbook -i inventory.yaml -e @vault --ask-vault-pass playbooks/$playbook.yaml
```
## Creating Secrets using Ansible Vault

To create secrets using ansible vault create a file and store your secrets in a key value format:

```
vault_dd_api_key: "********************"
````
Then run the following command to encrypt the file

```
ansible-vault encrypt vault.yml
```
You will be prompted to provide a password so that you can access the file. DO NOT loose this password or you will be unable to reopen the vault. 

To edit or view the vault run the following command
```
ansible-vault edit vault
```